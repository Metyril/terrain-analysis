#ifndef VEC2_H
#define VEC2_H

class Vec2{
public :
    double x, y;

    Vec2(double x = 0.0, double y = 0.0) : x(x), y(y) {}

    Vec2 operator+(const Vec2& v) const;
    Vec2 operator-(const Vec2& v) const;
    Vec2 operator-() const;
    Vec2 operator*(const double k);
    double operator*(const Vec2& v);
    double operator[](int i) const;
    double& operator[](int i);

    double Length() const;
    Vec2 Normalized() const;

    Vec2 Scale(const double k);
    Vec2 Scale(const Vec2& v);
    Vec2 Translate(const double k);
    Vec2 Translate(const Vec2& v);
    Vec2 Rotate(const double theta);
};

#endif
