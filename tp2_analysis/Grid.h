#ifndef GRID_H
#define GRID_H

#include "Box2.h"

class Grid : public Box2 {
protected:
    int nx, ny;
    Vec2 diagonal;
    Vec2 cellDiagonal;
    Vec2 inverseCellDiagonal;
    Vec2 inverseCellDiagonalSquared;

public:
    Grid(const Box2& box = Box2::Empty, int nx = 0, int ny = 0) : Box2(box), nx(nx), ny(ny) {
        diagonal = b - a;
        cellDiagonal = diagonal.Scale(Vec2(1.0 / double(nx - 1), 1.0 / double(ny - 1)));
        inverseCellDiagonal = Vec2(1.0 / cellDiagonal[0], 1.0 / cellDiagonal[1]);
        inverseCellDiagonalSquared = Vec2(inverseCellDiagonal[0] * inverseCellDiagonal[0], inverseCellDiagonal[1] * inverseCellDiagonal[1]);
    }

    bool Inside(int, int) const;
    bool Border(int, int) const;
    int Index(int i, int j) const;
    int getNx() const;
    int getNy() const;
    Vec2 Vertex(int i, int j) const;
};

#endif
