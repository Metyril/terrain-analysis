#include "Vec2.h"
#include <math.h>

Vec2 Vec2::operator+(const Vec2& v) const {
    return Vec2(x + v.x, y + v.y);
}

Vec2 Vec2::operator-(const Vec2& v) const {
    return Vec2(x - v.x, y - v.y);
}

Vec2 Vec2::operator-() const {
    return Vec2(-x, -y);
}

Vec2 Vec2::operator*(const double k) {
    return Vec2(x * k, y * k);
}

double Vec2::operator*(const Vec2& v) {
    return x * v.x + y * v.y;
}

double Vec2::operator[](int i) const {
    return (i == 0) ? x : y;
}

double& Vec2::operator[](int i) {
    return (i == 0) ? x : y;
}

double Vec2::Length() const {
    return sqrt(x * x + y * y);
}

Vec2 Vec2::Normalized() const {
    Vec2 myVec = (*this);
    return myVec * (1.0 / Length());
}

Vec2 Vec2::Scale(const double k) {
    return Vec2(x * k, y * k);
}

Vec2 Vec2::Scale(const Vec2& v) {
    return Vec2(x * v.x, y * v.y);
}

Vec2 Vec2::Translate(const double k) {
    return Vec2(x + k, y + k);
}

Vec2 Vec2::Translate(const Vec2& v) {
    return Vec2(x + v.x, y + v.y);
}

Vec2 Vec2::Rotate(const double theta) {
  double sa = sin(theta);
  double ca = cos(theta);
  return Vec2(x * ca - y * sa, x * sa + y * ca);
}
