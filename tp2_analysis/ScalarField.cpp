#include "ScalarField.h"

const double blur[3][3] = {
    {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
    {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
    {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0}
};

const double smooth[3][3] = {
    {1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0},
    {2.0 / 16.0, 4.0 / 16.0, 2.0 / 16.0},
    {1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0}
};

double& ScalarField::at(int i, int j) {
    return field[Index(i, j)];
}

double ScalarField::at(int i, int j) const {
    return field[Index(i, j)];
}

double& ScalarField::atSave(int i, int j) {
    return fieldSave[Index(i, j)];
}

double ScalarField::atSave(int i, int j) const {
    return fieldSave[Index(i, j)];
}

void ScalarField::Import(const QImage& image, double minHauteur, double maxHauteur) {
    for (int i = 0; i < image.height(); i++) {
        for (int j = 0; j < image.width(); j++) {
            this->at(i, j) = image.pixelColor(i, j).red() * (maxHauteur - minHauteur) / 255.0 + minHauteur;

            if (this->at(i, j) > maxValue) {
                this->maxValue = this->at(i, j);
            }

            if (this->at(i, j) < minValue) {
                this->minValue = this->at(i, j);
            }
        }
    }

    SaveField();
}

void ScalarField::Export(QString filename) {
    shade.save(filename);
}

void ScalarField::SaveField() {
    fieldSave = field;
}

Vec2 ScalarField::Gradient(int i, int j) const  {
    Vec2 n;

    if (i == 0) {
        n[0] = (at(i + 1, j) - at(i, j)) * inverseCellDiagonal[0];
    } else if (i == nx - 1) {
        n[0] = (at(i, j) - at(i - 1, j)) * inverseCellDiagonal[0];
    } else {
        n[0] = (at(i + 1, j) - at(i - 1, j)) * 0.5 * inverseCellDiagonal[0];
    }

    if (j == 0) {
        n[1] = (at(i, j + 1) - at(i, j)) * inverseCellDiagonal[1];
    } else if (j == ny - 1) {
        n[1] = (at(i, j) - at(i, j - 1)) * inverseCellDiagonal[1];
    } else {
        n[1] = (at(i, j + 1) - at(i, j - 1)) * 0.5 * inverseCellDiagonal[1];
    }

    return n;
}

double ScalarField::GradientNorm(int i, int j) {
    Vec2 g = Gradient(i, j);
    return qSqrt(g * g);
}

double ScalarField::Laplacian(int i, int j) const {
    double laplacian = 0.0;

    if (i == 0) {
        laplacian += (at(i, j) - 2.0 * at(i + 1, j) + at(i + 2, j)) * inverseCellDiagonalSquared[0];
    } else if (i == nx - 1) {
        laplacian += (at(i, j) - 2.0 * at(i - 1, j) + at(i - 2, j)) * inverseCellDiagonalSquared[0];
    } else {
        laplacian += (at(i + 1, j) - 2.0 * at(i, j) + at(i - 1, j)) * inverseCellDiagonalSquared[0];
    }

    if (j == 0) {
        laplacian += (at(i, j) - 2.0 * at(i, j + 1) + at(i, j + 2)) * inverseCellDiagonalSquared[1];
    } else if (j == ny - 1) {
        laplacian += (at(i, j) - 2.0 * at(i, j - 1) + at(i, j - 2)) * inverseCellDiagonalSquared[1];
    } else {
        laplacian += (at(i, j + 1) - 2.0 * at(i, j) + at(i, j - 1)) * inverseCellDiagonalSquared[1];
    }

    return laplacian;
}


void ScalarField::Blur() {
    SaveField();
    ApplyFilter(blur);
}

void ScalarField::Smooth() {
    SaveField();
    ApplyFilter(smooth);
}

void ScalarField::ApplyFilter(const double filter[3][3]) {
    for (int i = 1; i < nx - 1; i++) {
        for (int j = 1; j < ny - 1; j++) {
            for (int h = i - 1; h < i + 1; h++) {
                for (int w = j - 1; w < j + 1; w++) {
                    this->at(i, j) += filter[h + 1 - i][w + 1 - j] * this->atSave(h, w);
                }
            }
        }
    }
}

void ScalarField::RemoveFilter() {
    field = fieldSave;
}

double ScalarField::Normalize(double x) {
    return x / qMax(maxValue, -minValue);
}

double ScalarField::Clamp(double x, double minValue, double maxValue) {
    return qMin(qMax(x, minValue), maxValue);
}
