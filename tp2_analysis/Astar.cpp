#include "Astar.h"

float aStar::calcDist(QPoint &p) {
    int x = end.x() - p.x(), y = end.y() - p.y();
    return(x * x + y * y);
}

bool aStar::isValid(QPoint &p){
    return (p.x() > -1 && p.y() > -1 && p.x() < hf.getNx() && p.y() < hf.getNy());
}

bool aStar::existPoint(QPoint &p, int cost) {
    std::list<node>::iterator i;

    i = std::find( closed.begin(), closed.end(), p );
    if( i != closed.end() ) {
        if( ( *i ).cost + ( *i ).dist < cost ) return true;
        else { closed.erase( i ); return false; }
    }
    i = std::find( open.begin(), open.end(), p );
    if( i != open.end() ) {
        if( ( *i ).cost + ( *i ).dist < cost ) return true;
        else { open.erase( i ); return false; }
    }

    return false;
}

float aStar::cost(QPoint neighbour, node n) {
    float a = n.pos.x() - neighbour.x();
    float b = n.pos.y() - neighbour.y();
    float depCost = qSqrt(a * a + b * b);
    double h = hf.Normalize(hf.Height(n.pos.x(), n.pos.y()));
    if (h > 0.03 && h < 0.1) {
        depCost += 1;
    }
    return depCost + 10 * (0.5 + std::abs(hf.Height(n.pos.x(), n.pos.y()) - hf.Height(neighbour.x(), neighbour.y())));
}

bool aStar::addingNeighbour(QPoint neighbour, node n) {
    float stepCost, nc, dist;

    if( neighbour == end ) return true;

    if( isValid( neighbour )) {
        stepCost = cost(neighbour, n);
        nc = stepCost + n.cost;
        dist = calcDist( neighbour );
        if( !existPoint( neighbour, nc + dist ) ) {
            node m;
            m.cost = nc; m.dist = dist;
            m.pos = neighbour;
            m.parent = n.pos;
            open.push_back( m );
        }
    }

    return false;
}

bool aStar::fillOpen(node &n, int k) {
    QPoint neighbour;

    for (int i = n.pos.x() - 1; i <= n.pos.x() + 1; i++) {
        for (int j = n.pos.y() - 1; j <= n.pos.y() + 1; j++) {
            if (i != n.pos.x() || j != n.pos.y()) {
                neighbour = QPoint(i, j);
                if (addingNeighbour(neighbour, n)) return true;
            }
        }
    }
    int l = 2;
    while(l <= k) {
        int a = 1;
        for(int i = 0; i < 4; i++) {
            QPoint neighbours[4];
            neighbours[0] = QPoint(1, 0); neighbours[2] = QPoint(-1,0);
            neighbours[1] = QPoint(0, 1); neighbours[3] = QPoint(0, -1);
            while(a < l) {
                neighbour = n.pos + neighbours[i] * l;
                if(i % 2 == 0) {
                    neighbour.setY(n.pos.y() + a);
                    if (addingNeighbour(neighbour, n)) return true;
                    neighbour.setY(n.pos.y() - a);
                    if (addingNeighbour(neighbour, n)) return true;
                } else {
                    neighbour.setX(n.pos.x() + a);
                    if (addingNeighbour(neighbour, n)) return true;
                    neighbour.setX(n.pos.x() - a);
                    if (addingNeighbour(neighbour, n)) return true;
                }
                a++;
            }
        }
        l++;
    }

    return false;
}

bool aStar::search(QPoint &s, QPoint &e, int k){
    node n;
    end = e;
    start = s;
    n.cost = 0;
    n.pos = s;
    n.parent = QPoint();
    n.dist = calcDist(s);
    open.push_back(n);

    while( !open.empty() ) {
        open.sort();
        node n = open.front();
        open.pop_front();
        closed.push_back(n);
        if(fillOpen(n, k)) return true;
    }

    return false;
}

void aStar::path(std::list<QPoint> &path) {
    path.push_front( end );
    path.push_front( closed.back().pos );
    QPoint parent = closed.back().parent;

    for( std::list<node>::reverse_iterator i = closed.rbegin(); i != closed.rend(); i++ ) {
        if( ( *i ).pos == parent && !( ( *i ).pos == start ) ) {
            path.push_front( ( *i ).pos );
            parent = ( *i ).parent;
        }
    }
    path.push_front( start );
}
