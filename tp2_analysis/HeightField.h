#ifndef HEIGHTFIELD_H
#define HEIGHTFIELD_H

#include "ScalarField.h"
#include "Vec3.h"

#include <QVector>
#include <QPoint>
#include <QPainter>
#include <math.h>

enum Shaders {HEIGHT = 1, SLOPE, LAPLACIAN, PHONG, STREAM_AREA, STREAM_POWER, WETNESS_INDEX};

class ScalarPoint2 {
protected:
    QPoint p;
    double z;
public:
    ScalarPoint2() {}
    ScalarPoint2(const QPoint& point, const double& d) : p(point), z(d) {}
    friend bool operator<(const ScalarPoint2& a, const ScalarPoint2& b) { return a.z < b.z; }

    QPoint Point() const { return p; };
    double Scalar() const { return z; };
};

class HeightField : public ScalarField {
public:
    HeightField() : ScalarField() {}
    HeightField(const ScalarField& s) : ScalarField(s) {}
    HeightField(const QImage& image, const Box2& box, double minHeight, double maxHeight) :
        ScalarField(Grid(box, image.width(), image.height())) {
        this->Import(image, minHeight, maxHeight);
    }

    double Height(int i, int j) const;
    double Slope(int i, int j) const;

    Vec3 Vertex(int i, int j) const;
    Vec3 Normal(int i, int j) const;

    ScalarField SlopeMap();
    ScalarField LaplacianMap();
    ScalarField StreamPowerMap();
    ScalarField WetnessIndexMap();

    QVector<ScalarPoint2> GetScalarPoints() const;
    int CheckFlowSlope(const QPoint& p, QPoint* point, double* height, double* slope, double* nslope) const;
    ScalarField StreamAreaSteepest() const;

    void Shade(const unsigned int mode);
    void HeightShade();
    void SlopeShade();
    void LaplacianShade();
    void PhongShade();
    void StreamAreaShade();
    void StreamPowerShade();
    void WetnessIndexShade();
    void RoadShade(std::list<QPoint> path);
};

#endif
