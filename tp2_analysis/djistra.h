#ifndef DJISTRA_H
#define DJISTRA_H

#include <iostream>
#include <vector>
#include <string>
#include <list>

#include <limits> // for numeric_limits

#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>

typedef int vertex_t;
typedef double weight_t;

struct neighbor {
    vertex_t target;
    weight_t weight;
    neighbor(vertex_t arg_target, weight_t arg_weight)
        : target(arg_target), weight(arg_weight) { }
};

const weight_t max_weight = std::numeric_limits<double>::infinity();

typedef std::vector<std::vector<neighbor>> adjacency_list_t;

#endif // DJISTRA_H
