#-------------------------------------------------
#
# Project created by QtCreator 2020-12-09T17:47:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tp2_analysis
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    Astar.cpp \
    DisplayWidget.cpp \
    Vec2.cpp \
    Vec3.cpp \
    Box2.cpp \
    Grid.cpp \
    ScalarField.cpp \
    HeightField.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    Astar.h \
    DisplayWidget.h \
    Vec2.h \
    Vec3.h \
    Box2.h \
    Grid.h \
    ScalarField.h \
    HeightField.h \
    mainwindow.h

FORMS += \
    mainwindow.ui
