#ifndef VEC3_H
#define VEC3_H

#include "Vec2.h"

class Vec3 {
public :
    double x, y, z;

    Vec3(double x = 0.0, double y = 0.0, double z = 0.0) : x(x), y(y), z(z) {}
    Vec3(const Vec2& v, double z = 0.0) : x(v.x), y(v.y), z(z) {}

    Vec3 operator+(const Vec3& v);
    Vec3 operator-(const Vec3& v);
    Vec3 operator-();
    Vec3 operator*(double d);
    double operator*(const Vec3& v);
    double operator[](int i) const;
    double& operator[](int i);

    double Length() const;
    Vec3 Normalized() const;

    Vec3 Scale(const double k);
    Vec3 Scale(const Vec3& v);
    Vec3 Translate(const double k);
    Vec3 Translate(const Vec3& v);
    Vec3 RotateX(const float theta);
    Vec3 RotateY(const float theta);
    Vec3 RotateZ(const float theta);
};

#endif
