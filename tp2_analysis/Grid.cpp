#include "Grid.h"

bool Grid::Inside(int i, int j) const {
    return (i >= 0) && (i < nx) && (j >= 0) && (j < ny);
}

bool Grid::Border(int i, int j) const {
    return (i == 0) || (i == nx - 1) || (j == 0) || (j == ny - 1);
}

int Grid::Index(int i, int j) const {
    return nx * j + i;
}

int Grid::getNx() const {
    return this->nx;
}

int Grid::getNy() const {
    return this->ny;
}

Vec2 Grid::Vertex(int i, int j) const {
    double u = double(i) / (nx - 1);
    double v = double(j) / (ny - 1);
    return Vec2((1 - u) * a[0] + u * b[0], (1 - v) * a[1] + v * b[1]);
}
