#include "Vec3.h"
#include <math.h>

Vec3 Vec3::operator+(const Vec3& v) {
    return Vec3(x + v.x, y + v.y, z + v.z);
}

Vec3 Vec3::operator-(const Vec3& v) {
    return Vec3(x - v.x, y - v.y, z - v.z);
}

Vec3 Vec3::operator-() {
    return Vec3(-x, -y, -z);
}

Vec3 Vec3::operator *(double d) {
    return Vec3(x * d, y * d, z * d);
}

double Vec3::operator*(const Vec3& v) {
    return x * v.x + y * v.y + z * v.z;
}

double Vec3::operator[](int i) const {
    if (i == 0) {
        return x;
    } else if (i == 1) {
        return y;
    }

    return z;
}

double& Vec3::operator[](int i) {
    if (i == 0) {
        return x;
    } else if (i == 1) {
        return y;
    }

    return z;
}

double Vec3::Length() const {
    return sqrt(x * x + y * y + z * z);
}

Vec3 Vec3::Normalized() const {
    Vec3 myVec = (*this);
    return myVec * (1.0 / Length());
}

Vec3 Vec3::Scale(const double k) {
    return Vec3(x * k, y * k, z * k);
}

Vec3 Vec3::Scale(const Vec3& v) {
    return Vec3(x * v.x, y * v.y, z * v.z);
}

Vec3 Vec3::Translate(const double k) {
    return Vec3(x + k, y + k, z + k);
}

Vec3 Vec3::Translate(const Vec3& v) {
    return Vec3(x + v.x, y + v.y, z + v.z);
}

Vec3 Vec3::RotateX(const float theta) {
  double sa = sin(theta);
  double ca = cos(theta);
  return Vec3(x, y * ca - z * sa, y * sa + z * ca);
}

Vec3 Vec3::RotateY(const float theta) {
  double sa = sin(theta);
  double ca = cos(theta);
  return Vec3(x * ca + z * sa, y, x * -sa + z * ca);
}

Vec3 Vec3::RotateZ(const float theta) {
  double sa = sin(theta);
  double ca = cos(theta);
  return Vec3(x * ca - y * sa, x * sa + y * ca, z);
}
