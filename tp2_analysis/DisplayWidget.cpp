#include "DisplayWidget.h"

const double max_weight = std::numeric_limits<double>::infinity();

void DijkstraComputePaths(int source,
                          const std::vector<std::vector<neighbor>> &adjacency_list,
                          std::vector<double> &min_distance,
                          std::vector<int> &previous)
{
    unsigned long long n = adjacency_list.size();
    min_distance.clear();
    min_distance.resize(n, max_weight);
    min_distance[source] = 0;
    previous.clear();
    previous.resize(n, -1);
    std::set<std::pair<double, int>> vertex_queue;
    vertex_queue.insert(std::make_pair(min_distance[source], source));

    while (!vertex_queue.empty())
    {
        double dist = vertex_queue.begin()->first;
        int u = vertex_queue.begin()->second;
        vertex_queue.erase(vertex_queue.begin());

        // Visit each edge exiting u
        const std::vector<neighbor> &neighbors = adjacency_list[u];
        for (std::vector<neighbor>::const_iterator neighbor_iter = neighbors.begin();
             neighbor_iter != neighbors.end();
             neighbor_iter++)
        {
                int v = neighbor_iter->target;
                double weight = neighbor_iter->weight;
                double distance_through_u = dist + weight;
            if (distance_through_u < min_distance[v]) {
                vertex_queue.erase(std::make_pair(min_distance[v], v));

                min_distance[v] = distance_through_u;
                previous[v] = u;
                vertex_queue.insert(std::make_pair(min_distance[v], v));
            }
        }
    }
}

std::list<int> DijkstraGetShortestPathTo(int vertex, const std::vector<int> &previous) {
    std::list<int> path;
    for (; vertex != -1; vertex = previous[vertex])
        path.push_front(vertex);
    return path;
}

void DisplayWidget::fillShortestPaths(std::vector<std::vector<neighbor>> &adjacency_list) {
    for (int i = 0; i < hf.getNy(); i++) {
        for (int j = 0; j < hf.getNx(); j++) {
            int index = i * hf.getNx() + j;

            if (index - hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index - hf.getNx(), cost(index, index - hf.getNx())));
            if (index + hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index + hf.getNx(), cost(index, index + hf.getNx())));
            if (index - 1 >= 0)
                adjacency_list[index].push_back(neighbor(index - 1, cost(index, index - 1)));
            if (index + 1 < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index + 1, cost(index, index + 1)));

            if (index - 1 - hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index - 1 - hf.getNx(), cost(index, index - 1 - hf.getNx())));
            if (index - 1 + hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index - 1 + hf.getNx(), cost(index, index - 1 + hf.getNx())));
            if (index + 1 - hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index + 1 - hf.getNx(), cost(index, index + 1 - hf.getNx())));
            if (index + 1 + hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index + 1 + hf.getNx(), cost(index, index + 1 + hf.getNx())));

            if (index - 1 - 2 * hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index - 1 - 2 * hf.getNx(), cost(index, index - 1 - 2 * hf.getNx())));
            if (index - 1 + 2 * hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index - 1 + 2 * hf.getNx(), cost(index, index - 1 + 2 * hf.getNx())));
            if (index + 1 - 2 * hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index + 1 - 2 * hf.getNx(), cost(index, index + 1 - 2 * hf.getNx())));
            if (index + 1 + 2 * hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index + 1 + 2 * hf.getNx(), cost(index, index + 1 + 2 * hf.getNx())));
            if (index - 2 - hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index - 2 - hf.getNx(), cost(index, index - 2 - hf.getNx())));
            if (index - 2 + hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index - 2 + hf.getNx(), cost(index, index - 2 + hf.getNx())));
            if (index + 2 - hf.getNx() >= 0)
                adjacency_list[index].push_back(neighbor(index + 2 - hf.getNx(), cost(index, index + 2 - hf.getNx())));
            if (index + 2 + hf.getNx() < hf.getNx() * hf.getNy())
                adjacency_list[index].push_back(neighbor(index + 2 + hf.getNx(), cost(index, index + 2 + hf.getNx())));
        }
    }
}

double DisplayWidget::cost(int index, int neighbor) {
    int x1 = index % hf.getNx();
    int y1 = index / hf.getNx();
    int x2 = neighbor % hf.getNx();
    int y2 = neighbor / hf.getNx();

    double cost = 0;
    cost += std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    cost += 10 * (0.5 + std::abs(hf.Height(x1, y1) - hf.Height(x2, y2)));
    cost += hf.Slope(x2, y2);
    if (hf.Normalize(hf.Height(x2, y2)) >= 0.25) {
        cost *= 100;
    } else if (hf.Normalize(hf.Height(x2, y2)) < 0.01) {
        cost = 1000;
    }

    return cost;
}

void DisplayWidget::on_Height_Released() {
    shadeMethod = HEIGHT;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Slope_Released() {
    shadeMethod = SLOPE;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Laplacian_Released() {
    shadeMethod = LAPLACIAN;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Phong_Released() {
    shadeMethod = PHONG;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_StreamArea_Released() {
    shadeMethod = STREAM_AREA;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_StreamPower_Released() {
    shadeMethod = STREAM_POWER;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_WetnessIndex_Released() {
    shadeMethod = WETNESS_INDEX;
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Road_Released() {
    int p1 = 487 + 375 * hf.getNx();
    int p2 = 564 + 416 * hf.getNx();

    std::vector<std::vector<neighbor>> adjacency_list(hf.getNx() * hf.getNy());
    std::vector<double> min_distance;
    std::vector<int> previous;

    fillShortestPaths(adjacency_list);
    DijkstraComputePaths(p1, adjacency_list, min_distance, previous);
    std::list<int> path = DijkstraGetShortestPathTo(p2, previous);
    std::list<QPoint> pathPoints;
    std::list<int>::iterator it;
    for (it = path.begin(); it != path.end(); ++it){
        pathPoints.push_back(QPoint((*it) % hf.getNx(), (*it) / hf.getNx()));
    }
    hf.RoadShade(pathPoints);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Export_Released() {
    hf.Export("../data/result.png");
}

void DisplayWidget::on_Load_Released() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open height map"), "../data/", tr("Image Files (*.png *.jpg *.bmp)"));
    heightMap.load(fileName);
    hf = HeightField(heightMap, Box2(Vec2(0, 0), Vec2(size, size)), 0, height);
    image->setPixmap(QPixmap::fromImage(heightMap));
}

void DisplayWidget::on_Size_Moved(int value) {
    size = value * 1000;
    hf = HeightField(heightMap, Box2(Vec2(0, 0), Vec2(size, size)), 0, height);
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}

void DisplayWidget::on_Height_Moved(int value) {
    height = value * 100;
    hf = HeightField(heightMap, Box2(Vec2(0, 0), Vec2(size, size)), 0, height);
    hf.Shade(shadeMethod);
    image->setPixmap(QPixmap::fromImage(hf.shade));
}
