#ifndef DISPLAYWIDGET_H
#define DISPLAYWIDGET_H

#include <QtWidgets>
#include <QLabel>
#include <QFileDialog>
#include <vector>
#include <list>
#include <limits> // for numeric_limits
#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>
#include "HeightField.h"
#include "Astar.h"

struct neighbor {
    int target;
    double weight;
    neighbor(int arg_target, double arg_weight) : target(arg_target), weight(arg_weight) {}
};

class DisplayWidget : public QWidget
{
    Q_OBJECT

public:
    DisplayWidget(QWidget *parent = 0) : QWidget(parent), shadeMethod(1), size(15000), height(1200) {
        image = new QLabel(this);
        image->setScaledContents(true);
        image->setGeometry(QRect(120,0,540,540));
    }

    void fillShortestPaths(std::vector<std::vector<neighbor>> &adjacency_list);
    double cost(int index, int neighbor);

public slots:
    void on_Height_Released();
    void on_Slope_Released();
    void on_Laplacian_Released();
    void on_Phong_Released();
    void on_StreamArea_Released();
    void on_StreamPower_Released();
    void on_WetnessIndex_Released();
    void on_Road_Released();
    void on_Export_Released();
    void on_Load_Released();
    void on_Size_Moved(int value);
    void on_Height_Moved(int value);

private:
    QLabel* image;
    QImage heightMap;
    HeightField hf;
    int shadeMethod;
    int size;
    int height;
};

#endif
