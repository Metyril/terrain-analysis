#ifndef SCALARFIELD_H
#define SCALARFIELD_H

#include "Grid.h"
#include <vector>

#include <QImage>
#include <QtMath>
#include <QString>

class ScalarField : public Grid {
public:
    std::vector<double> field;
    std::vector<double> fieldSave;
    QImage shade;
    double maxValue;
    double minValue;
public:
    ScalarField() : Grid() {}
    ScalarField(const Grid& grid, double value = 0.0) : Grid(grid) {
        shade = QImage(nx, ny, QImage::Format_ARGB32);
        field.resize(nx * ny, value);
        fieldSave.resize(nx * ny, value);
        maxValue = 0.0;
        minValue = 10000.0;
    }
    ScalarField(const ScalarField& sf) : Grid(Grid(Box2(sf.a, sf.b), sf.nx, sf.ny)) {
        field = sf.field;
        fieldSave = sf.fieldSave;
        shade = sf.shade;
        maxValue = sf.maxValue;
        minValue = sf.minValue;
    }

    double& at(int i, int j);
    double at(int i, int j) const;
    double& atSave(int i, int j);
    double atSave(int i, int j) const;

    void Import(const QImage& image, double minHauteur, double maxHauteur);
    void Export(QString filename);
    void SaveField();

    Vec2 Gradient(int i, int j) const;
    double GradientNorm(int i, int j);
    double Laplacian(int i, int j) const;

    void Smooth();
    void Blur();
    void ApplyFilter(const double filter[3][3]);
    void RemoveFilter();

    double Normalize(double x);
    double Clamp(double x, double minValue, double maxValue);
};

#endif
