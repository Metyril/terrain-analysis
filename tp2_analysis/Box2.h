#ifndef BOX2_H
#define BOX2_H

#include "Vec2.h"

class Box2 {
protected:
    Vec2 a,b;
public:
    Box2(const Vec2& a, const Vec2& b) : a(a), b(b) {};
    bool Inside(const Vec2& v) const;
    bool Intersect(const Box2& box) const;
    Vec2 Vertex(int k) const;
    static const Box2 Empty;
};

#endif
