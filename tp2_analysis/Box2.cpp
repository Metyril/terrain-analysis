#include "Box2.h"

bool Box2::Inside(const Vec2& v) const {
    return (v[0] >= a[0]) && (v[0] <= b[0]) && (v[1] >= a[1]) && (v[1] <= b[1]);
}

bool Box2::Intersect(const Box2& box) const {
    return (a[0] < box.b[0]) && (a[1] < box.b[1]) && (b[0] > box.a[0]) && (b[1] > box.a[1]);
}

Vec2 Box2::Vertex(int k) const {
    return Vec2((k & 1) ? b[0] : a[0], (k & 2) ? b[1] : a[1]);
}

const Box2 Box2::Empty(Vec2(0.0, 0.0), Vec2(0.0, 0.0));
