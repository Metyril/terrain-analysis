#include "HeightField.h"

const QPoint nextNeighbour[8] = {
    QPoint(1, 0), QPoint(1, 1), QPoint(0, 1), QPoint(-1, 1), QPoint(-1, 0), QPoint(-1, -1), QPoint(0, -1), QPoint(1, -1)
};

const double inverseDistance[8] = {
    1.0, 1.0 / sqrt(2.0), 1.0, 1.0 / sqrt(2.0), 1.0, 1.0 / sqrt(2.0), 1.0 / sqrt(2.0)
};

double HeightField::Height(int i, int j) const {
    return at(i, j);
}

double HeightField::Slope(int i, int j) const {
    Vec2 g = Gradient(i, j);
    return sqrt(g * g);
}

Vec3 HeightField::Vertex(int i, int j) const {
    return Vec3(Grid::Vertex(i, j), Height(i, j));
}

Vec3 HeightField::Normal(int i, int j) const {
    return Vec3(-Gradient(i, j), 1.0).Normalized();
}

ScalarField HeightField::SlopeMap() {
    ScalarField slope(Grid(Box2(a, b), nx, ny), 0.0);

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nx; j++) {
            slope.at(i, j) = Slope(i, j);

            if (slope.maxValue < slope.at(i, j)) {
                slope.maxValue = slope.at(i, j);
            }

            if (slope.minValue > slope.at(i, j)) {
                slope.minValue = slope.at(i, j);
            }

        }
    }

    return slope;
}

ScalarField HeightField::LaplacianMap() {
    ScalarField laplacian(Grid(Box2(a, b), nx, ny), 0.0);

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nx; j++) {
            laplacian.at(i, j) = Laplacian(i, j);

            if (laplacian.maxValue < laplacian.at(i, j)) {
                laplacian.maxValue = laplacian.at(i, j);
            }

            if (laplacian.minValue > laplacian.at(i, j)) {
                laplacian.minValue = laplacian.at(i, j);
            }

        }
    }

    return laplacian;
}

ScalarField HeightField::StreamPowerMap() {
    ScalarField streamPower(Grid(Box2(a, b), nx, ny), 0.0);
    ScalarField streamArea = StreamAreaSteepest();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            streamPower.at(i, j) = sqrt(streamArea.at(i, j)) * Slope(i, j);

            if (streamPower.maxValue < streamPower.at(i, j)) {
                streamPower.maxValue = streamPower.at(i, j);
            }

            if (streamPower.minValue > streamPower.at(i, j)) {
                streamPower.minValue = streamPower.at(i, j);
            }

        }
    }

    return streamPower;
}

ScalarField HeightField::WetnessIndexMap() {
    ScalarField wetnessIndex(Grid(Box2(a, b), nx, ny), 0.0);
    ScalarField streamArea = StreamAreaSteepest();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            wetnessIndex.at(i, j) = Clamp(log(streamArea.at(i, j)) / (Slope(i, j) + 0.0001), 0.0, 100.0);

            if (wetnessIndex.maxValue < wetnessIndex.at(i, j)) {
                wetnessIndex.maxValue = wetnessIndex.at(i, j);
            }

            if (wetnessIndex.minValue > wetnessIndex.at(i, j)) {
                wetnessIndex.minValue = wetnessIndex.at(i, j);
            }

        }
    }

    return wetnessIndex;
}

void HeightField::Shade(const unsigned int mode) {
    switch (mode) {
        case HEIGHT: HeightShade(); break;
        case SLOPE: SlopeShade(); break;
        case LAPLACIAN: LaplacianShade(); break;
        case PHONG: PhongShade(); break;
        case STREAM_AREA: StreamAreaShade(); break;
        case STREAM_POWER: StreamPowerShade(); break;
        case WETNESS_INDEX: WetnessIndexShade(); break;
        default : break;
    }
}

void HeightField::HeightShade() {
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double h = Normalize(Height(i, j));
            shade.setPixel(i, j, qRgb(255 * h, 255 * h, 255 * h));
        }
    }
}

void HeightField::SlopeShade() {
    ScalarField slope = SlopeMap();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double s = slope.Normalize(slope.at(i, j));
            shade.setPixel(i, j, qRgb(255 * s, 255 * s, 255 * s));
        }
    }
}

void HeightField::LaplacianShade() {
    ScalarField laplacian = LaplacianMap();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double l = laplacian.Normalize(laplacian.at(i, j));

            double color = Clamp(255 * abs(l) * 3, 0, 255);
            shade.setPixel(i, j, qRgb(color, color, color));
        }
    }
}

void HeightField::PhongShade() {
    const Vec3 lightdir = Vec3(2.0, 1.0, 4.0).Normalized();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            Vec3 n = Normal(i, j);
            double d = n * lightdir;
            d = (1.0 + d) * 0.5;
            d *= d;

            double height = Normalize(Height(i, j));
            QRgb color = qRgb(200 * d + 10, 200 * d + 10, 200 * d + 10);
            if (height < 0.03) {
                color = qRgb(173, 216, 230);
            } else if (height < 0.04) {
                color = qRgb(211 * d + 10,199 * d +10, 162 * d + 10);
            } else if (height < 0.1) {
                color = qRgb(155 * d + 10, 158 * d + 10, 161 * d + 10);
            } else if (height < 0.3) {
                color = qRgb(164 * d + 10, 216 * d + 10, 177 * d + 10);
            } else if (height < 0.9) {
                color = qRgb(170 * d + 10, 122 * d + 10, 96 * d + 10);
            } else {
                int c = Clamp(255 * d + 10, 0, 255);
                color = qRgb(c, c, c);
            }

            shade.setPixel(i, j, color);
        }
    }
}

void HeightField::StreamAreaShade() {
    PhongShade();
    ScalarField streamArea = StreamAreaSteepest();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double s = streamArea.Normalize(streamArea.at(i, j));

            if (s > 0.02) {
                shade.setPixel(i, j, qRgb(255, 0, 0));
            }
        }
    }
}

void HeightField::StreamPowerShade() {
    PhongShade();
    ScalarField streamPower = StreamPowerMap();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double sP = streamPower.Normalize(streamPower.at(i, j));

            if (sP > 0.02) {
                shade.setPixel(i, j, qRgb(255, 0, 0));
            }
        }
    }
}

void HeightField::WetnessIndexShade() {
    PhongShade();
    ScalarField wetnessIndex = WetnessIndexMap();

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            double wI = wetnessIndex.Normalize(wetnessIndex.at(i, j));

            if (wI > 0.02) {
                shade.setPixel(i, j, qRgb(255, 0, 0));
            }
        }
    }
}

void HeightField::RoadShade(std::list<QPoint> path) {
    PhongShade();

    QPainter painter(&shade);
    QPen pen;
    pen.setWidth(3);
    pen.setColor(Qt::blue);
    painter.setPen(pen);

    std::list<QPoint>::iterator it;
    for (it = path.begin(); it != path.end();){
        QPoint q1 = *it;
        std::advance(it, 1);
        if (it != path.end()) {
            QPoint q2 = *it;
            painter.drawLine(q1, q2);
        }
    }
    painter.end();
}

QVector<ScalarPoint2> HeightField::GetScalarPoints() const {
    QVector<ScalarPoint2> points(nx * ny);

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            points[Index(i, j)] = ScalarPoint2(QPoint(i, j), at(i, j));
        }
    }

    return points;
}

ScalarField HeightField::StreamAreaSteepest() const {
    ScalarField stream(Grid(Box2(a, b), nx, ny), 1.0);

    QVector<ScalarPoint2> points = this->GetScalarPoints();
    std::sort(points.begin(), points.end());

    for (int i = points.size() - 1; i >= 0; i--) {
        QPoint point = points.at(i).Point();
        QPoint neighbours[8];
        double heights[8];
        double slopes[8];
        double slopesNormalized[8];

        int nbTargets = CheckFlowSlope(point, neighbours, heights, slopes, slopesNormalized);

        if (nbTargets > 0) {
            double maxSlope = slopes[0];
            int maxIndex = 0;
            for (int j = 1; j < nbTargets; j++) {
                if (slopes[j] > maxSlope) {
                    maxIndex = j;
                    maxSlope = slopes[j];
                }
            }

            stream.at(neighbours[maxIndex].x(), neighbours[maxIndex].y()) += stream.at(point.x(), point.y());
        }
    }

    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            if (stream.at(i, j) > stream.maxValue) {
                stream.maxValue = stream.at(i, j);
            }

            if (stream.at(i, j) < stream.minValue) {
                stream.minValue = stream.at(i, j);
            }
        }
    }

    return stream;
}

int HeightField::CheckFlowSlope(const QPoint& p, QPoint* points, double* heights, double* slopes, double* slopesNormalized) const {
    int nbTargets = 0;
    double z = at(p.x(), p.y());
    double slopeSum = 0.0;

    for (int i = 0; i < 8; i++) {
        QPoint neighbour = p + nextNeighbour[i];

        if (!Inside(neighbour.x(), neighbour.y())) {
            continue;
        }

        double step = at(neighbour.x(), neighbour.y()) - z;
        if (step < 0.0) {
            points[nbTargets] = neighbour;
            heights[nbTargets] = -step;
            slopes[nbTargets] = -step * inverseDistance[i];
            slopeSum += slopes[nbTargets];
            nbTargets++;
        }
    }

    for (int k = 0; k < nbTargets; k++) {
        slopesNormalized[k] = slopes[k] / slopeSum;
    }

    return nbTargets;
}
