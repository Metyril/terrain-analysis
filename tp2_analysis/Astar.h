#ifndef ASTAR_H
#define ASTAR_H

#include "HeightField.h"

#include <list>
#include <algorithm>
#include <iostream>

class node {
public:
    bool operator == (const node& o ) { return pos == o.pos; }
    bool operator == (const QPoint& o ) { return pos == o; }
    bool operator < (const node& o ) { return dist + cost < o.dist + o.cost; }

    QPoint pos, parent;
    int dist;
    float cost;
};

class aStar {
public:
    aStar(HeightField hfi) : hf(hfi) {}

    float calcDist(QPoint& p);

    bool isValid(QPoint& p);

    bool existPoint(QPoint& p, int cost);

    float cost(QPoint neighbour, node n);

    bool addingNeighbour(QPoint neighbour, node n);

    bool fillOpen(node& n, int k );

    bool search(QPoint& s, QPoint& e, int k = 1);

    void path(std::list<QPoint>& path );

    HeightField hf;
    QPoint end, start;
    std::list<node> open;
    std::list<node> closed;
};

#endif
