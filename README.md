# Terrain Analysis

Nous avons réalisé notre projet sous Qt.

## Comment compiler le projet

Installer Qt Creator : https://www.qt.io/download

Ouvrir un projet existant et sélectionner le fichier : `tp2_analysis/tp2_analysis.pro`.

Paramètres de l'application :
- `../data/fichier.extension` ou autre chemin d'accès pour charger une heightmap;
- `-size X` pour définir la taille de la Box. Par défaut = 15000;
- `-height X` pour définir la hauteur maximale du terrain. Par défaut = 1200;

Exécutez le code avec le raccourci `Ctrl + R`.
